This is a repository containing a task for the internship candidates at yameo.

This repository contains simple Java based project.
The code in this project breaks some of the good design principles - SOLID, DRY, etc. and contains some bugs.

The assignment task is as follows:

1. Please refactor this code so that it follows good design patterns / principles. 
Please fix bugs as you find them. This example is very short, so your solution may seem robust and in some cases
over-engineered. This is understandable and will not have a negative impact on the final evaluation of your work.
2. **Please do not rewrite the whole solution from the beginning**. 
Please do work on the existing code and refactor it. 
Please document your changes step by step in the way you find the most convenient.
3. Please add at least one additional shape to the application (you may try adding one before and one after your modifications). 
4. The **deadline** for this assignment is **January the 7th 2018 23:59 CET**. 
Please submit your answer in the way you find the most convenient (email, github / bitbucket / other repo).
5. In case you don't know how the solution should work / behave, 
please use your best judgement and very shortly document / describe the reason behind your choice.

GLHF!

03.01.2018

First I tested how the program works and go through the whole project. Next steps are as follows:

1. I fixed few bugs:
    the triangle could be made with wrong set of edges, so I add a simple constraint as a function
    which checks dimensions and return Triangle or throw an exception. Then I realized that there was a hint in a test.
    So I change the kind of exception in test and now it works fine.

    The last of square tests was wrongly written. When upcasting the object (in the test square is assigned to rectangle-parent)
    polimorphic functions are used from the lowest - child object. So, the Rectangle behave like a square.
    I add a method which upcast a square to Rectangular functionality - by returning a new object based on particular square.

    In menu, during creating a new shape there wasn't a solution to step back to the main menu.
    I add a throwable object which is doing it.

2. I add a Regular Pentagon. It's the most simply one :)

3. I separated a shapeList. I made a class which is only for data storing, adding and listing.
    I also made a class to use this structure. For now it doesn't make sense, but I'll leave it.
    It could be helpful when connecting the structure with Hibernate.
    Unfortunately I won't found out how it could works wih Hibernate until next week.

4. I change a name of interface from Shape to IShape. Also I separated these methods,
    because in some point of app development maybe someone will want to add lines, point or empty circles.

5. Separating interfaces cause some problems in shape printing procedure.
    I made a functions which print a standard form with shape details and implement it in code. This function behave in 
    various way, depending on the kind of object. So let's say I make a use of Strategy design pattern. 
    I gather it with reading and printing from console function to another class. Just to make the main class more compact.

6. There's still a bug in passing through menu. When choosing an index to get from the shape list.

7. Functions which modify and create shapes should be made is some more simple way. But I'm not sure if it won't be
    classified as rewriting the solution from the beginning.
    
04.01.2018

Let's try to add a line to the program!
    It's possible. A line doesn't have a perimeter, so I add an interface for line and another option for printing shape.
    Thanks to the strategy pattern it was quite easy.

1. I fix a bud in modifying the shape. While choosing shape by index there wasn't a protection againt too big index or -1. 
    So I add a throw clause and catch clause in menu loop.
    
2. I create an Shape Abstract Factory to keep a list of available shapes.
3. ShapeFactory has a single-responsibility - it only create shapes, and there' only one function for that.
    Thanks that GeometryApp lost another responsibility. And it doesn't use x methods to create particular shape. 
    oups.. Shape Abstract Factory is now useless.
    
4. ShapeFactory is also modyfing shapes already created. Don't know if it can be in the same class, 
    so I'll leave it in the same. Unfortunately there's a must to give an access to structure in a parameter of modifying method.

5. I've noticed that there was another bug in the first version. While modyfing the shapes, in the first else clause 
    active shape was to Ellipse. So the second check was always false. In my solution this problem is avoid because 
    shapes are identified by Enum. Thanks to Enum a code is more transparent. Now it's possible to use switch form.
    And te most important - from the App level we don't need to have an access to the lowest classes.
    
6. I pack functions which are used in App to one Interface, just to give a sign that I'm using specific functions.

On my current state of knowleadge I think that's all I can do.
That was nice exercise, thanks.