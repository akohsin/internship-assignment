package pl.yameo.internship.assignment.shapescreation;

import pl.yameo.internship.assignment.shapes.WrongTrianglesMeasuresException;
import pl.yameo.internship.assignment.date.IGetShapeByIndex;
import pl.yameo.internship.assignment.shapes.*;

public class ShapeFactory extends ShapeAbstractFactory {
    private IGetShapeByIndex functions;

    public ShapeFactory(IGetShapeByIndex functions) {
        this.functions = functions;
    }

    public IShape createNewShape(SHAPE_TYPE shape_type, Double... dimensions)
            throws NoSuchShapeTypeException, WrongTrianglesMeasuresException {
        switch (shape_type) {
            case LINE: {
                return new Line(dimensions[0]);
            }
            case CIRCLE: {
                return new Circle(dimensions[0]);
            }
            case SQUARE: {
                return new Square(dimensions[0]);
            }
            case ELLIPSE: {
                return new Ellipse(dimensions[0], dimensions[1]);
            }
            case RECTANGLE: {
                return new Rectangle(dimensions[0], dimensions[1]);
            }
            case REGULARPENTAGON: {
                return new RegularPentagon(dimensions[0]);
            }
            case TRIANGLE: {
                return new Triangle(dimensions[0], dimensions[1], dimensions[2]);
            }
            default: {
                throw new NoSuchShapeTypeException();
            }
        }
    }

    public void modifyShape(int index, Double... newDimensions) throws NoSuchShapeTypeException, WrongTrianglesMeasuresException {
        IShape activeShape = functions.getShapeByIndex(index);
        SHAPE_TYPE shape_type = activeShape.getShapeType();
        switch (shape_type) {
            case LINE: {
                ((Line) activeShape).setLength(newDimensions[0]);
                break;
            }
            case CIRCLE: {
                ((Circle) activeShape).setRadius(newDimensions[0]);
                break;
            }
            case SQUARE: {
                ((Square) activeShape).setDimension(newDimensions[0]);
                break;
            }
            case ELLIPSE: {
                ((Ellipse) activeShape).setSemiMajorAxis(newDimensions[0]);
                ((Ellipse) activeShape).setSemiMinorAxis(newDimensions[1]);
                break;
            }
            case RECTANGLE: {
                ((Rectangle) activeShape).setHeight(newDimensions[0]);
                ((Rectangle) activeShape).setWidth(newDimensions[1]);
                break;
            }
            case REGULARPENTAGON: {
                ((RegularPentagon) activeShape).setEdge(newDimensions[0]);
                break;
            }
            case TRIANGLE: {
                Triangle activeTriangle = (Triangle) activeShape;
                activeTriangle.checkAndSetDimensions(newDimensions[0], newDimensions[1], newDimensions[2]);
                break;
            }
            default: {
                throw new NoSuchShapeTypeException();
            }
        }
    }
}
