package pl.yameo.internship.assignment.shapescreation;

public enum SHAPE_TYPE {
    ELLIPSE, CIRCLE, LINE, RECTANGLE, REGULARPENTAGON, SQUARE, TRIANGLE
}
