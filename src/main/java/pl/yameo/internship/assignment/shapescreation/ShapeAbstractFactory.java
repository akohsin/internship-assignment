package pl.yameo.internship.assignment.shapescreation;


import java.util.ArrayList;
import java.util.List;

abstract class ShapeAbstractFactory {

    private List<SHAPE_TYPE> shape_types = new ArrayList<>();

    public ShapeAbstractFactory() {
        for (SHAPE_TYPE x : SHAPE_TYPE.values()) {
            shape_types.add(x);
        }

    }

    protected List<SHAPE_TYPE> getShape_types() {
        return shape_types;
    }
}
