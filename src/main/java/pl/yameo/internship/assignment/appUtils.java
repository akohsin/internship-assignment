package pl.yameo.internship.assignment;

import pl.yameo.internship.assignment.shapes.IArea;
import pl.yameo.internship.assignment.shapes.ILength;
import pl.yameo.internship.assignment.shapes.IPerimeter;
import pl.yameo.internship.assignment.shapes.IShape;

import java.util.Scanner;

public class appUtils {
    public static Integer readInteger(Scanner scanner) {
        Integer value = null;
        while (value == null) {
            if (scanner.hasNextInt()) {
                value = scanner.nextInt();
            } else {
                System.out.println("Give appropriate Integer type");
                scanner.next();
            }
        }

        return value;
    }

    public static Double readDouble(Scanner scanner) {
        Double value = null;
        while (value == null) {
            if (scanner.hasNextDouble()) {
                System.out.println("Give appropriate Double type");
                value = scanner.nextDouble();
            } else {
                scanner.next();
            }
        }

        return value;
    }

    public static String printShape(IShape shape) {
        StringBuilder shapeToPrint = new StringBuilder();

        shapeToPrint.append(shape.getName() + " with dimensions: ");
        shapeToPrint.append(shape.listDimensions() + "; ");
        if (shape instanceof IArea) {
            IArea shapeTmp = (IArea) shape;
            shapeToPrint.append("Area: " + shapeTmp.calculateArea() + "; ");
        }
        if (shape instanceof IPerimeter) {
            IPerimeter shapeTmp = (IPerimeter) shape;
            shapeToPrint.append("Perimeter: " + shapeTmp.calculatePerimeter() + "; ");
        }
        if (shape instanceof ILength) {
            ILength shapeTmp = (ILength) shape;
            shapeToPrint.append("Length: " + shapeTmp.getLength() + "; ");
        }
        shapeToPrint.append("\n");
        return shapeToPrint.toString();
    }
}
