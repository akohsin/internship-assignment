package pl.yameo.internship.assignment.shapes;

import pl.yameo.internship.assignment.shapescreation.SHAPE_TYPE;

import java.util.Arrays;
import java.util.List;

public class Triangle implements IShape, IPerimeter, IArea {
    private Double edgeA = 0.0;
    private Double edgeB = 0.0;
    private Double edgeC = 0.0;

    public Triangle(Double edgeA, Double edgeB, Double edgeC) throws WrongTrianglesMeasuresException {
        checkAndSetDimensions(edgeA, edgeB, edgeC);

    }

    public void checkAndSetDimensions(Double edgeA, Double edgeB, Double edgeC) throws WrongTrianglesMeasuresException {
        boolean isPossible = false;
        if (edgeA + edgeB > edgeC) {
            if (edgeA + edgeC > edgeB) {
                if (edgeB + edgeC > edgeA) {
                    this.edgeA = edgeA;
                    this.edgeB = edgeB;
                    this.edgeC = edgeC;
                    isPossible = true;
                }
            }
        }
        if (!isPossible) {
            throw new WrongTrianglesMeasuresException();
        }
    }


    @Override
    public SHAPE_TYPE getShapeType() {
        return SHAPE_TYPE.TRIANGLE;
    }

    @Override
    public String getName() {
        return "Triangle";
    }

    @Override
    public final List<Double> listDimensions() {
        return Arrays.asList(edgeA, edgeB, edgeC);
    }

    @Override
    public final Double calculateArea() {
        Double halfPerimeter = calculatePerimeter() / 2;
        return Math.sqrt(halfPerimeter * (halfPerimeter - edgeA) * (halfPerimeter - edgeB) * (halfPerimeter - edgeC));
    }

    @Override
    public final Double calculatePerimeter() {
        return edgeA + edgeB + edgeC;
    }

    private void setEdgeA(Double edgeA) {
        this.edgeA = edgeA;
    }

    private void setEdgeB(Double edgeB) {
        this.edgeB = edgeB;
    }

    private void setEdgeC(Double edgeC) {
        this.edgeC = edgeC;
    }
}
