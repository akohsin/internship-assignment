package pl.yameo.internship.assignment.shapes;

import pl.yameo.internship.assignment.shapescreation.SHAPE_TYPE;

public class Circle extends Ellipse {
    public Circle(Double radius) {
        super(radius, radius);
    }

    @Override
    public String getName() {
        return "Circle";
    }

    @Override
    public void setSemiMajorAxis(Double semiMajorAxis) {
        setRadius(semiMajorAxis);
    }

    @Override
    public void setSemiMinorAxis(Double semiMinorAxis) {
        setRadius(semiMinorAxis);
    }

    public void setRadius(Double radius) {
        super.setSemiMajorAxis(radius);
        super.setSemiMinorAxis(radius);
    }

    @Override
    public SHAPE_TYPE getShapeType() {
        return SHAPE_TYPE.CIRCLE;
    }
}
