package pl.yameo.internship.assignment.shapes;

import pl.yameo.internship.assignment.shapescreation.SHAPE_TYPE;

import java.util.List;

public interface IShape {

    SHAPE_TYPE getShapeType();

    String getName();

    List<Double> listDimensions();

}
