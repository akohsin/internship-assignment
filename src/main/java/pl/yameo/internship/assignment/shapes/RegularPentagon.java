package pl.yameo.internship.assignment.shapes;

import pl.yameo.internship.assignment.shapescreation.SHAPE_TYPE;

import java.util.Arrays;
import java.util.List;

public class RegularPentagon implements IShape, IArea, IPerimeter {
    private double edge = 0.0;

    public RegularPentagon(double edge) {
        this.edge = edge;
    }

    @Override
    public Double calculateArea() {
        return (edge * edge * Math.sqrt(5 * (5 + 2 * Math.sqrt(5)))) / 4;
    }

    @Override
    public Double calculatePerimeter() {
        return 5 * edge;
    }

    @Override
    public SHAPE_TYPE getShapeType() {
        return SHAPE_TYPE.REGULARPENTAGON;
    }

    @Override
    public String getName() {
        return "Regular Pentagon";
    }

    @Override
    public List<Double> listDimensions() {
        return Arrays.asList(edge);
    }

    public void setEdge(double edge) {
        this.edge = edge;
    }
}
