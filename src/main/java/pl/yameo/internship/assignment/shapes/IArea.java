package pl.yameo.internship.assignment.shapes;

public interface IArea {
    Double calculateArea();
}
