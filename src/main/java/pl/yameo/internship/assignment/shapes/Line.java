package pl.yameo.internship.assignment.shapes;

import pl.yameo.internship.assignment.shapescreation.SHAPE_TYPE;

import java.util.Arrays;
import java.util.List;

public class Line implements IShape, ILength {
    Double length = 0.0;

    public Line(Double length) {
        this.length = length;
    }

    public void setLength(Double length) {
        this.length = length;
    }

    @Override
    public Double getLength() {
        return length;
    }

    @Override
    public SHAPE_TYPE getShapeType() {
        return SHAPE_TYPE.LINE;
    }

    @Override
    public String getName() {
        return "Segment line";
    }

    @Override
    public List<Double> listDimensions() {
        return Arrays.asList(length);
    }
}
