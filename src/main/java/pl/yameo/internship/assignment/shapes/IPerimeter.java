package pl.yameo.internship.assignment.shapes;

public interface IPerimeter {
    Double calculatePerimeter();
}
