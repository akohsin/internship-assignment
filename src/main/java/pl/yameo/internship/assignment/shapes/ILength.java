package pl.yameo.internship.assignment.shapes;

public interface ILength {
    Double getLength();
}
