package pl.yameo.internship.assignment.date;

import pl.yameo.internship.assignment.shapes.IShape;
import pl.yameo.internship.assignment.shapescreation.SHAPE_TYPE;

public interface IFunctions {
    String listShapes();

    IShape getShapeByIndex(int index);

    int shapeListSize();

    void addShape(IShape newShape);

    SHAPE_TYPE getTypeOfShapeByIndex(int index);
}
