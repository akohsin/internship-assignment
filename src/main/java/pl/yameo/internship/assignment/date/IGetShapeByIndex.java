package pl.yameo.internship.assignment.date;

import pl.yameo.internship.assignment.shapes.IShape;

public interface IGetShapeByIndex {
    IShape getShapeByIndex(int index);
}
