package pl.yameo.internship.assignment.date;

import pl.yameo.internship.assignment.shapes.IArea;
import pl.yameo.internship.assignment.shapes.IPerimeter;
import pl.yameo.internship.assignment.shapes.IShape;

import java.util.ArrayList;

class ShapesStructure {
    private ArrayList<IShape> shapeList = new ArrayList<>();

    @Override
    public String toString() {
        StringBuilder listedShapes = new StringBuilder();
        shapeList.forEach(shape -> {
            listedShapes.append(shape.getName() + " with dimensions: ");
            listedShapes.append(shape.listDimensions() + "; ");
            if (shape instanceof IArea) {
                IArea shapeTmp = (IArea) shape;
                listedShapes.append("Area: " + shapeTmp.calculateArea() + "; ");
            }
            if (shape instanceof IPerimeter) {
                IPerimeter shapeTmp = (IPerimeter) shape;
                listedShapes.append("Perimeter: " + shapeTmp.calculatePerimeter() + "\n");
            }
        });
        return listedShapes.toString();
    }

    public void addShape(IShape shape) {
        shapeList.add(shape);
    }

    public int shapeListSize() {
        return shapeList.size();
    }

    public IShape getShape(int index) {
        return shapeList.get(index);
    }
}
