package pl.yameo.internship.assignment.date;

import pl.yameo.internship.assignment.shapes.IShape;
import pl.yameo.internship.assignment.shapescreation.SHAPE_TYPE;

public class ShapesStructureFunctions implements IGetShapeByIndex, IFunctions {
    private ShapesStructure shapesStructure = new ShapesStructure();

    public String listShapes() {
        return shapesStructure.toString();
    }

    public void addShape(IShape newShape) {
        shapesStructure.addShape(newShape);
    }

    public int shapeListSize() {
        return shapesStructure.shapeListSize();
    }

    public IShape getShapeByIndex(int index) {
        return shapesStructure.getShape(index);
    }

    public SHAPE_TYPE getTypeOfShapeByIndex(int index) {
        return getShapeByIndex(index).getShapeType();
    }
}
