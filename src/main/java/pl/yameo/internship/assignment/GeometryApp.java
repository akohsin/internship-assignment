package pl.yameo.internship.assignment;

import pl.yameo.internship.assignment.date.IFunctions;
import pl.yameo.internship.assignment.date.IGetShapeByIndex;
import pl.yameo.internship.assignment.date.ShapesStructureFunctions;
import pl.yameo.internship.assignment.shapes.*;
import pl.yameo.internship.assignment.shapescreation.NoSuchShapeTypeException;
import pl.yameo.internship.assignment.shapescreation.SHAPE_TYPE;
import pl.yameo.internship.assignment.shapescreation.ShapeFactory;

import java.util.Scanner;

import static pl.yameo.internship.assignment.appUtils.printShape;
import static pl.yameo.internship.assignment.appUtils.readDouble;
import static pl.yameo.internship.assignment.appUtils.readInteger;

public class GeometryApp {
    private Scanner scanner;
    private IFunctions functions = new ShapesStructureFunctions();
    private ShapeFactory shapeFactory = new ShapeFactory((IGetShapeByIndex) functions);

    public GeometryApp(Scanner scanner) {
        this.scanner = scanner;
    }

    public void start() {
        boolean run = true;
        while (run) {
            run = run();
        }
    }

    private boolean run() {
        System.out.println("Choose action:");
        System.out.println("1) Create new shape");
        System.out.println("2) List existing shapes");
        System.out.println("3) Modify one of the shapes from the list");
        System.out.println("0) Exit");

        int option = readInteger(scanner);
        if (option == 0) {
            return false;
        } else if (option == 1) {
            try {
                functions.addShape(createNewShape());
            } catch (WrongOptionNumberException e) {
                System.out.println("No shape with this option number");
            } catch (BackMenuOption bmo) {
            } catch (WrongTrianglesMeasuresException e) {
                System.out.println("These 3 line segments cannot be connected to a triangle ");
            } catch (NoSuchShapeTypeException e) {
                System.out.println("There's no such shape type in app code");
            }

        } else if (option == 2) {
            System.out.println("====== LIST OF SHAPES ======");
            System.out.println(functions.listShapes());
            System.out.println("============================");
        } else if (option == 3) {
            try {
                modifyShape();
            } catch (WrongTrianglesMeasuresException e) {
                System.out.println("These 3 line segments cannot be connected to a triangle ");
            } catch (WrongOptionNumberException e) {
                System.out.println("There's no shape with this index");
            } catch (NoSuchShapeTypeException e) {
                System.out.println("There's no such shape type in app code");
            }
        }
        return true;
    }

    private IShape createNewShape()
            throws WrongOptionNumberException, BackMenuOption, WrongTrianglesMeasuresException, NoSuchShapeTypeException {
        System.out.println("Choose shape to create:");
        System.out.println("1) Ellipse");
        System.out.println("2) Rectangle");
        System.out.println("3) Circle");
        System.out.println("4) Square");
        System.out.println("5) Triangle");
        System.out.println("6) Regular Pentagon");
        System.out.println("7) Line");
        System.out.println("0) Back");

        int option = readInteger(scanner);

        if (option == 1) {
            System.out.println("Please provide two semi-axis lengths (major, minor):");
            return shapeFactory.createNewShape(SHAPE_TYPE.ELLIPSE, readDouble(scanner), readDouble(scanner));
        } else if (option == 2) {
            System.out.println("Please provide two edge lengths (height, width):");
            return shapeFactory.createNewShape(SHAPE_TYPE.RECTANGLE, readDouble(scanner), readDouble(scanner));
        } else if (option == 3) {
            System.out.println("Please provide the radius for the circle:");
            return shapeFactory.createNewShape(SHAPE_TYPE.CIRCLE, readDouble(scanner));
        } else if (option == 4) {
            System.out.println("Please provide the edge length:");
            return shapeFactory.createNewShape(SHAPE_TYPE.SQUARE, readDouble(scanner));
        } else if (option == 5) {
            System.out.println("Please provide three edge lengths:");
            return shapeFactory.createNewShape(SHAPE_TYPE.TRIANGLE, readDouble(scanner), readDouble(scanner), readDouble(scanner));
        } else if (option == 6) {
            System.out.println("Please provide one edge length");
            return shapeFactory.createNewShape(SHAPE_TYPE.REGULARPENTAGON, readDouble(scanner));
        } else if (option == 7) {
            System.out.println("Please provide a length of this line");
            return shapeFactory.createNewShape(SHAPE_TYPE.LINE, readDouble(scanner));
        } else if (option == 0) {
            throw new BackMenuOption();
        } else {
            throw new WrongOptionNumberException();
        }
    }


    private void modifyShape() throws WrongTrianglesMeasuresException, WrongOptionNumberException, NoSuchShapeTypeException {
        System.out.println("============================");
        System.out.println(functions.listShapes());
        System.out.println();
        System.out.println("Please choose the index of the shape you want to modify (1-" + functions.shapeListSize() + "): ");

        int index = readInteger(scanner);

        if (index > functions.shapeListSize() || index == 0) {
            throw new WrongOptionNumberException();
        }
        String oldShape = printShape(functions.getShapeByIndex(index - 1));

        System.out.println(oldShape);

        switch (functions.getTypeOfShapeByIndex(index - 1)) {
            case LINE: {
                System.out.println("Please provide a length of this line");
                shapeFactory.modifyShape(index - 1, readDouble(scanner));
                break;
            }
            case CIRCLE: {
                System.out.println("Please provide the radius for the circle:");
                shapeFactory.modifyShape(index - 1, readDouble(scanner));
                break;
            }
            case SQUARE: {
                System.out.println("Please provide the edge length:");
                shapeFactory.modifyShape(index - 1, readDouble(scanner));
                break;
            }
            case ELLIPSE: {
                System.out.println("Please provide two semi-axis lengths (major, minor):");
                shapeFactory.modifyShape(index - 1, readDouble(scanner), readDouble(scanner));
                break;
            }
            case RECTANGLE: {
                System.out.println("Please provide two edge lengths (height, width):");
                shapeFactory.modifyShape(index - 1, readDouble(scanner), readDouble(scanner));
                break;
            }
            case REGULARPENTAGON: {
                System.out.println("Please provide one edge length");
                shapeFactory.modifyShape(index - 1, readDouble(scanner));
                break;
            }
            case TRIANGLE: {
                System.out.println("Please provide three edge lengths:");
                shapeFactory.modifyShape(index - 1, readDouble(scanner), readDouble(scanner), readDouble(scanner));
                break;
            }
            default: {
                throw new NoSuchShapeTypeException();
            }
        }
        System.out.println("============================");
        System.out.println("Old shape: ");
        System.out.print(oldShape);
        System.out.println("============================");

        System.out.println("New shape: ");
        System.out.print(printShape(functions.getShapeByIndex(index - 1)));
        System.out.println("============================");

    }
}

